class FriendCirclesController < ApplicationController
  def new
    @cirlce = FriendCircle.new
  end

  def create
    @circle = FriendCircle.new(params[:friend_circle])

    if @circle.save
      render :json => @circle
    else
      flash[:errors] = @circle.errors.full_messages
      render :new
    end
  end

  def edit
    @circle = FriendCircle.find(params[:id])
  end

  def update
    @circle = FriendCircle.find_by_id(params[:id])

    if @circle.update_attributes(params[:friend_circle])
      render :json => @circle
    else
      flash[:errors] = @circle.errors.full_messages
      render :edit
    end
  end
end
