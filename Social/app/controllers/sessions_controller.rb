class SessionsController < ApplicationController

  def create
    @user = User.find_by_credentials(params[:user][:email], params[:user][:password])

    if @user
      log_in!(@user)
      redirect_to new_session_url
    else
      render :text => "Wrong Credentials"
    end
  end

  def destroy
    log_out!
    if current_user.save
      redirect_to new_session_url
    else
      render :text => "Log out fail!"
    end
  end


end
