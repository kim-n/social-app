class UsersController < ApplicationController

  def create
    @user = User.new(params[:user])

    if @user.save
      render :text => "#{@user} saved"
    else
      flash.now[:errors] = @user.errors.full_messages
      render :new
    end

  end


  def reset_password
    @user = User.find(params[:id])
    if @user.password_token == params[:password_token]
      render :reset_password
    else
      flash[:errors] = ["Inaccurate link"]
      redirect_to reset_users_url
    end
  end


  def update
    @user = User.find(params[:id])

    if params[:user][:password].blank?
      flash[:errors] = ["Password can't be blank"]
      redirect_to reset_password_users_url(params[:email_params])
    else
      @user.update_attributes(params[:user])
      @user.password_token = nil
      @user.save
      render :json => @user
    end
  end

  def request_password_reset
    @user = User.find_by_email(params[:user][:email])

    if @user
      send_reset_email(@user)

      flash[:errors] = ["Email Sent"]
      redirect_to reset_users_url
    else
      flash[:errors] = ["No such email"]
      redirect_to reset_users_url
    end
  end

  def reset
    render :reset
  end


end
