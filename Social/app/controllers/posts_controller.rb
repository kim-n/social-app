class PostsController < ApplicationController

  def new

  end

  def create
    @post = Post.new(params[:post])
    params[:link].each do |link|
      @post.links.new(url: link) unless link.empty?
    end

    if @post.save
      render :json => @post
    else
      render :json => @post.errors.full_messages

    end
  end

end
