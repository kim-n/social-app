module SessionsHelper

  def log_in!(user)
    session[:token] = user.session_token
    current_user = user
  end

  def current_user
    @current_user ||= User.find_by_session_token(session[:token])
  end

  def current_user=(user)
    @current_user = user
  end

  def log_out!
    current_user.reset_session_token
    session[:token] = nil
    # current_user.save!
  end
end
