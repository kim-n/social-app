module UsersHelper

  def send_reset_email(user)
    user.set_password_token
    user.save!
    msg = UserMailer.reset_email(user)
    msg.deliver!
  end
end
