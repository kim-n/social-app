class User < ActiveRecord::Base
  attr_accessible :email, :password, :session_token, :password_token

  validates :email, :password_digest, :session_token, presence: true
  before_validation :ensure_session_token


  has_many(
    :memberships,
    :class_name => "FriendCircleMembership",
    :foreign_key => :user_id,
    :primary_key => :id
  )

  has_many(
    :circles,
    through: :memberships,
    source: :friend_circle
  )


  def password=(password)
    self.password_digest = BCrypt::Password.create(password) unless password.blank?
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def self.generate_session_token
    SecureRandom.urlsafe_base64
  end


  def set_password_token
    self.password_token = SecureRandom.urlsafe_base64
  end

  def ensure_session_token
    self.session_token ||= self.reset_session_token
  end

  def self.find_by_credentials(email, password)
    @user = User.find_by_email(email)

    if !@user.nil? && @user.is_password?(password)
      @user
    else
      nil
    end
  end

  def reset_session_token
    self.session_token = User.generate_session_token
  end

end
