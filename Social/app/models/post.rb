class Post < ActiveRecord::Base
  attr_accessible :user_id, :body, :circle_ids

  validates :user_id, :body, presence: true

  has_many(
    :links,
    class_name: "Link",
    foreign_key: :post_id,
    primary_key: :id,
    inverse_of: :post
  )

  has_many(
    :post_shares,
    class_name: "PostShare",
    foreign_key: :post_id,
    primary_key: :id,
    inverse_of: :post
  )

  has_many(
    :circles,
    through: :post_shares,
    source: :circle
  )
end
