class FriendCircle < ActiveRecord::Base
  attr_accessible :name, :member_ids

  validates :name, presence: true

  has_many(
    :memberships,
    :class_name => "FriendCircleMembership",
    :foreign_key => :friend_circle_id,
    :primary_key => :id
  )

  has_many(
    :members,
    through: :memberships,
    source: :user
  )

  has_many(
    :post_shares,
    class_name: "PostShare",
    foreign_key: :circle_id,
    primary_key: :id,
    inverse_of: :circle
  )

end
