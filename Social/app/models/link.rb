class Link < ActiveRecord::Base
  attr_accessible :url, :post_id

  validates :url, :post, presence: true

  belongs_to(
    :post,
    class_name: "Post",
    foreign_key: :post_id,
    primary_key: :id,
    inverse_of: :links
  )

end
