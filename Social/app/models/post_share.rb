class PostShare < ActiveRecord::Base
  attr_accessible :circle_id, :post_id

  validates :post_id, :circle_id, presence: true

  belongs_to(
    :post,
    class_name: "Post",
    foreign_key: :post_id,
    primary_key: :id,
    inverse_of: :post_shares
  )

  belongs_to(
    :circle,
    class_name: "FriendCircle",
    foreign_key: :circle_id,
    primary_key: :id,
    inverse_of: :post_shares
  )

end
