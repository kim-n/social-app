class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  def reset_email(user)
    @user = user
    @url = reset_password_users_url({ password_token: user.password_token, id: user.id })
    mail(to: user.email, subject: "Change your password")
  end
end